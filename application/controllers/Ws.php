<?php

use Restserver\libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Ws extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Modelmahasiswa');
    }

    public function index()
    {
        $mahasiswa = $this->Modelmahasiswa->get_mahasiswa();
        echo json_encode($mahasiswa);
    }

    public function status()
    {
        $this->load->view('test');
    }
}