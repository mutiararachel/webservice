<?php

class Modelmahasiswa extends CI_Model
{
    public function get_mahasiswa()
    {
    
        $mahasiswa = [
        [
            'id' => '123',
            'nama' => 'namatest',
            'nim' => '456893',
            'status' => 'OK'
        ],
        [
            'id' => '456',
            'nama' => 'namatestsatu',
            'nim' => '456893',
            'status' => 'OK'
        ],
        [
            'id' => '789',
            'nama' => 'namatestdua',
            'nim' => '456893',
            'status' => 'OK'
        ]
    ];
        return $mahasiswa;
    }
}